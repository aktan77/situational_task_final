# Project CI/CD Pipeline

This repository contains a CI/CD pipeline for building, testing, and deploying a Dockerized application using GitLab CI/CD.

## Pipeline Stages

The pipeline consists of three stages:
1. **Build**
2. **Test**
3. **Deploy**

## Pipeline Configuration

The pipeline is configured using a `.gitlab-ci.yml` file. Below is a detailed description of each stage and job.

### Build Stage

**Job**: `build_app`

This job builds the Docker image for the application and pushes it to the specified Docker repository.

- **Before Script**:
  - Logs in to Docker using credentials stored in environment variables (`$DOCKER_USERNAME` and `$DOCKER_TOKEN`).
- **Script**:
  - Changes directory to `app/Front`.
  - Builds the Docker image using the tag specified in the environment variable `$DOCKER_REPO_TAG`.
  - Pushes the Docker image to the repository.

```yaml
build_app:
  stage: build
  before_script:
    - echo $DOCKER_TOKEN | docker login --username $DOCKER_USERNAME --password-stdin
  script:
    - cd app/Front
    - docker image build -t $DOCKER_REPO_TAG .
    - echo "Pushing the image."
    - docker push $DOCKER_REPO_TAG
```

### Deploy Stage

**Job**: `deploy_app`

This job deploys the application to a Google Kubernetes Engine (GKE) cluster.

- **Image**: `google/cloud-sdk:latest`
- **Before Script**:
  - Decodes the base64-encoded Google Cloud service account credentials stored in the environment variable `$BASE64_GOOGLE_CLOUD_CREDENTIALS`.
  - Saves the credentials to a file (`service-account-key.json`).
  - Authenticates with Google Cloud using the service account key file.
  - Configures the Google Cloud project.
  - Logs in to Google Cloud.
- **Script**:
  - Retrieves the credentials for the specified GKE cluster.
  - Applies the Kubernetes configurations stored in the `/k8s` directory.

```yaml
deploy_app:
  stage: deploy
  image: google/cloud-sdk:latest
  before_script:
    - export GOOGLE_CLOUD_CREDENTIALS=$(echo $BASE64_GOOGLE_CLOUD_CREDENTIALS | base64 -d)
    - echo $GOOGLE_CLOUD_CREDENTIALS > service-account-key.json 
    - gcloud auth activate-service-account --key-file service-account-key.json 
    - gcloud config set project playground-s-11-f9973a89 
    - gcloud auth login
  script:
    - gcloud container clusters get-credentials cluster-1 --zone us-central1-c --project playground-s-11-f9973a89
    - kubectl apply -f /k8s
  environment: test
```

## Environment Variables

The pipeline relies on several environment variables for configuration:

- **Docker**:
  - `DOCKER_USERNAME`: Docker Hub username.
  - `DOCKER_TOKEN`: Docker Hub token for authentication.
  - `DOCKER_REPO_TAG`: Tag for the Docker image to be built and pushed.

- **Google Cloud**:
  - `BASE64_GOOGLE_CLOUD_CREDENTIALS`: Base64-encoded Google Cloud service account credentials.
  - `GOOGLE_CLOUD_PROJECT`: Google Cloud project ID.
  - `GKE_CLUSTER_NAME`: Name of the GKE cluster.
  - `GKE_CLUSTER_ZONE`: Zone of the GKE cluster.

## Prerequisites

Ensure you have the following set up before running the pipeline:

1. **Docker Hub**:
   - A Docker Hub account with a repository to push the built images.
   - Generate a Docker Hub access token and store it in the environment variable `$DOCKER_TOKEN`.

2. **Google Cloud**:
   - A Google Cloud project with a GKE cluster.
   - A service account with sufficient permissions and its credentials encoded in base64 format, stored in the environment variable `$BASE64_GOOGLE_CLOUD_CREDENTIALS`.

## Running the Pipeline

To run the pipeline, push your code to the GitLab repository. The pipeline will automatically start, executing the stages in the following order:

1. **Build**: Builds and pushes the Docker image.
2. **Deploy**: Deploys the application to the GKE cluster.

### Result
## Screenshots
Pipeline output
![alt text](image.png)
The final image in Dockerhub
![alt text](image-1.png)
https://hub.docker.com/repository/docker/aktan55/st_final_react/general

## Video records
https://drive.google.com/drive/folders/1H4UGXsEM3ZYzNhaaDdIUlwuehczXtD0e?usp=drive_link


## Conclusion

This README provides an overview of the CI/CD pipeline configured in the `.gitlab-ci.yml` file. Make sure to set up the required environment variables and prerequisites to ensure the pipeline runs smoothly.